fs = require('fs');
http = require('http');
url = require('url');


http.createServer(function (req, res) {
    var request = url.parse(req.url, true);
    var action = request.pathname;

    var img = fs.readFileSync('./logo.png');
    res.writeHead(200, {'Content-Type': 'image/png' });
    res.end(img, 'binary');

}).listen(8080, '127.0.0.1');
console.log("Serving on port 8080.");